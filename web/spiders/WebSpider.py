# coding=utf-8

import scrapy
from scrapy import Request
from web.items import WebItem


class WebSpider(scrapy.Spider):
    name = 'web_crawl'
    start_urls = ['http://www.job98.com/faq/4-317.aspx']
    PAGE = 318
    
    def parse(self, response):
        body = response.css('ul.exp')[0]
        body_items = body.xpath('./li')

        item = WebItem()

        def parse_action(body):
            # get username
            item['user'] = body.xpath('./div[@class="sub_title"]/div[@class="name"]/text()').extract_first()

            # get timestamp
            text = body.xpath('./div[@class="sub_title"]/div[@class="date"]/text()').extract_first()
            try:
                if u"发表" in text:
                    print text
                    item['time'] = text.strip().split(u'发表')[0]
                else:
                    item['time'] = text.strip()

                # get ip
                text = body.xpath('./div[@class="sub_title"]/div[@class="area"]/@title').extract_first()
                item['ip'] = text.strip().split(u'IP地址：')[1]

            except TypeError, e:
                print e

            # get ask content
            item['ask'] = body.xpath('./div[@class="ask"]/text()').extract_first()

            # get answer content
            item['answer'] = body.xpath('./div[@class="answer"]/text()').extract_first()

        for element in body_items:
            parse_action(element)
            yield item

        # NextPage
        next_page = response.css('.NextPage::attr(href)').extract_first()
        next_page_url = 'http://www.job98.com/faq/4-%s.aspx' % self.PAGE
        print next_page_url
        self.PAGE += 1

        yield Request(url = next_page_url, callback = self.parse, encoding = 'utf-8')
