# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import MySQLdb
from settings import MYSQL

class WebPipeline(object):
    def __init__(self):
       self.db = MySQLdb.connect(host=MYSQL['MYSQL_HOST'], user=MYSQL['MYSQL_USER'],
            passwd=MYSQL['MYSQL_PASSWD'], db=MYSQL['MYSQL_DBNAME'], charset='utf8', use_unicode=True)
       self.cursor = self.db.cursor()
       
    def process_item(self, item, spider):
        try:
            base_sql = 'INSERT INTO %s' % MYSQL['MYSQL_TABLENAME']
            self.cursor.execute(base_sql + """(user, time, ip, ask, answer) VALUES (%s, %s, %s, %s, %s)
                """, (item['user'], item['time'], item['ip'], item['ask'], item['answer']))
        except Exception, e:
            print e
            try:
                print(self.cursor._last_executed)
            except AttributeError, e:
                pass
        
        self.db.commit()
        return item
