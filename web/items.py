# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class WebItem(scrapy.Item):
    # define the fields for your item here like:
    user = scrapy.Field()
    time = scrapy.Field()
    ip = scrapy.Field()
    ask = scrapy.Field()
    answer = scrapy.Field()
    
