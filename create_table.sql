CREATE TABLE Web (
    id int(11) not null primary key auto_increment,
    user char(20) not null,
    time DATETIME,
    ip varchar(16),
    ask text not null,
    answer text
)